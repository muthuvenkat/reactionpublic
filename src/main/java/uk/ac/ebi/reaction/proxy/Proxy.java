package uk.ac.ebi.reaction.proxy;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author venkat
 * @date 03/11/15
 */
public class Proxy extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getContent(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getContent(request, response);
	}

	public static void getContent(HttpServletRequest request, HttpServletResponse response) {

		try {
			String reqUrl = request.getParameter("url");

			if (!reqUrl.contains("rhea-db.org")
					&& !reqUrl.contains("ebi.ac.uk")) return;

			URL url = new URL(reqUrl);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod(request.getMethod());

			if (request.getMethod().equals("POST"))
				con.setRequestProperty("Content-Type", request.getContentType());

			int clength = request.getContentLength();
			if(clength > 0) {
				con.setDoInput(true);
				byte[] idata = new byte[clength];
				request.getInputStream().read(idata, 0, clength);
				con.getOutputStream().write(idata, 0, clength);
			}
			response.setContentType(con.getContentType());
			BufferedReader rd = new BufferedReader(
					new InputStreamReader(con.getInputStream(),"UTF-8"));
			String line;
			PrintWriter out = response.getWriter();
			while ((line = rd.readLine()) != null) {
				out.println(line);
			}
			rd.close();
		} catch(Exception e) {
			e.printStackTrace();
			response.setStatus(500);
		}
	}
}
