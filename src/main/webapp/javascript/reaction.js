"use strict";
function search(mol, type, coef, simType) {
    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    var url = base_url+"/reactions/api/search/rxn/";

    if(type === 'similar'){
        var raw = "{\n	\"coef\":\""+ coef+"\",\
                   \n   \"query\":"+ JSON.stringify(mol) +"\n}";
        myHeaders.append("Content-Type", "application/json");
        url =url + type+"/"+simType
    } else {
        var raw = mol;
        myHeaders.append("Content-Type", "text/plain");
        url= url + type

    }
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    timeout(30000, fetch(url, requestOptions))
        .then(response => response.text())
        .then(result => {
            vue.results = JSON.parse(result);
            if (vue.results.length === 0)
                vue.noResult = true;
        })
        .catch(error => console.log('error', error));

}

function timeout(ms, promise) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject(new Error("timeout"))
        }, ms)
        promise.then(resolve, reject)
    })
}

function searchMol(mol, type, coef){
    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");

    if(type === 'similar'){
        var raw = "{\n	\"coef\":\""+ coef+"\",\
                   \n   \"query\":"+ JSON.stringify(mol) +"\n}";
        myHeaders.append("Content-Type", "application/json");
    } else {
        var raw = mol;
        myHeaders.append("Content-Type", "text/plain");

    }
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    timeout(30000,fetch(base_url+"/reactions/api/search/structure/"+type, requestOptions))
        .then(response => response.text())
        .then(result => {
            vue.results = JSON.parse(result);
            if (vue.results.length === 0)
                vue.noResult = true;
        })
        .catch(error => console.log('error', error));

}

function searchById(id) {
    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Content-Type", "text/plain");

    var formdata = new FormData();

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
    };

    timeout(30000,fetch(base_url+"/reactions/api/search/rxn/text?query="+id, requestOptions))
        .then(response => response.text())
        .then(result => {
            vue.results = JSON.parse(result);
            if (vue.results.length === 0)
                vue.noResult = true;
            else if( !vue.results[0].hasOwnProperty('chebiIds'))
                vue.selected = 'mol';
        })
        .catch(error => console.log('error', error));

}

var base_url = "http://ves-hx-93:8080";
// var url = "http://localhost:8080";